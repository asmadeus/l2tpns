Overview
========

`l2tpns` is half of a complete L2TP implementation. It supports only the
LNS side of the connection.

L2TP (Layer 2 Tunneling Protocol) is designed to allow any layer 2
protocol (e.g. Ethernet, PPP) to be tunneled over an IP connection.
`l2tpns` implements PPP over L2TP only.

There are a couple of other L2TP implementations, of which
[l2tpd](http://sourceforge.net/projects/l2tpd) is probably the most
popular. l2tpd also will handle being either end of a tunnel, and is a
lot more configurable than `l2tpns`. However, due to the way it works,
it is nowhere near as scalable.

`l2tpns` uses the TUN/TAP interface provided by the Linux kernel to
receive and send packets. Using some packet manipulation it doesn\'t
require a single interface per connection, as l2tpd does.

This allows it to scale extremely well to very high loads and very high
numbers of connections.

It also has a plugin architecture which allows custom code to be run
during processing. An example of this is in the walled garden module
included.

Installation
============

Requirements {#installation-requirements}
------------

-   Linux kernel version 2.4 or above, with the Tun/Tap interface either
    compiled in, or as a module.

-   libcli 1.8.5 or greater. You can get this from
    [SourceForge](http://sourceforge.net/projects/libcli)

Compiling {#installation-compile}
---------

You can generally get away with just running `make` from the source
directory. This will compile the daemon, associated tools and any
modules shipped with the distribution.

Installing {#installation-install}
----------

After you have successfully compiled everything, run `make install` to
install it. By default, the binaries are installed into `/usr/sbin`, the
configuration into `/etc/l2tpns`, and the modules into
`/usr/lib/l2tpns`.

You will definately need to edit the configuration files before you
start. See [Configuration](#configuration) for more information.

Running {#installation-run}
-------

You only need to run `/usr/sbin/l2tpns` as root to start it. It does not
normally detach to a daemon process (see the `-d` option), so you should
perhaps run it from `init`.

By default there is no log destination set, so all log messages will go
to stdout.

Configuration
=============

All configuration of the software is done from the files installed into
`/etc/l2tpns`.

`startup-config` {#config-startup}
----------------

This is the main configuration file for `l2tpns`. The format of the file
is a list of commands that can be run through the command-line
interface. This file can also be written directly by the `l2tpns`
process if a user runs the `write memory` command, so any comments will
be lost. However if your policy is not to write the config by the
program, then feel free to comment the file with a `#` or `!` at the
beginning of the line.

A list of the possible configuration directives follows. Each of these
should be set by a line like: set configstring \"value\" set ipaddress
192.168.1.1 set boolean true

`debug` (int)

:   Sets the level of messages that will be written to the log file. The
    value should be between 0 and 5, with 0 being no debugging, and 5
    being the highest. A rough description of the levels is:

    `0`: Critical Errors

    :   Things are probably broken

    `1`: Errors

    :   Things might have gone wrong, but probably will recover

    `2`: Warnings

    :   Just in case you care what is not quite perfect

    `3`: Information

    :   Parameters of control packets

    `4`: Calls

    :   For tracing the execution of the code

    `5`: Packets

    :   Everything, including a hex dump of all packets processed\...
        probably twice

    Note that the higher you set the debugging level, the slower the
    program will run. Also, at level 5 a *lot* of information will be
    logged. This should only ever be used for working out why it
    doesn\'t work at all.

`log_file` (string)

:   This will be where all logging and debugging information is written
    to. This may be either a filename, such as `/var/log/l2tpns`, or the
    special magic string `syslog:`facility, where facility is any one of
    the syslog logging facilities, such as `local5`.

`pid_file` (string)

:   If set, the process id will be written to the specified file. The
    value must be an absolute path.

`random_device` (string)

:   Path to random data source (default `/dev/urandom`). Use \"\" to use
    the rand() library function.

`l2tp_secret` (string)

:   The secret used by `l2tpns` for authenticating tunnel request. Must
    be the same as the LAC, or authentication will fail. Only actually
    be used if the LAC requests authentication.

`l2tp_mtu` (int)

:   MTU of interface for L2TP traffic (default: `1500`). Used to set
    link MRU and adjust TCP MSS.

`ppp_restart_time` (int); `ppp_max_configure` (int); `ppp_max_failure` (int)

:   PPP counter and timer values, as described in §4.1 of
    [RFC1661](ftp://ftp.rfc-editor.org/in-notes/rfc1661.txt).

`primary_dns` (ip address); `econdary_dns` (ip address)

:   Whenever a PPP connection is established, DNS servers will be sent
    to the user, both a primary and a secondary. If either is set to
    0.0.0.0, then that one will not be sent.

`primary_radius` (ip address); `secondary_radius` (ip address)

:   Sets the RADIUS servers used for both authentication and accounting.
    If the primary server does not respond, then the secondary RADIUS
    server will be tried.

    ::: {.note}
    In addition to the source IP address and identifier, the RADIUS
    server *must* include the source port when detecting duplicates to
    suppress (in order to cope with a large number of sessions coming
    on-line simultaneously `l2tpns` uses a set of udp sockets, each with
    a separate identifier).
    :::

`primary_radius_port` (short); `secondary_radius_port` (short)

:   Sets the authentication ports for the primary and secondary RADIUS
    servers. The accounting port is one more than the authentication
    port. If no RADIUS ports are given, the authentication port defaults
    to 1812, and the accounting port to 1813.

`radius_accounting` (boolean)

:   If set to true, then RADIUS accounting packets will be sent. This
    means that a Start record will be sent when the session is
    successfully authenticated, and a Stop record will be sent when the
    session is closed.

`radius_interim` (int)

:   If `radius_accounting` is on, defines the interval between sending
    of RADIUS interim accounting records (in seconds).

`radius_secret` (string)

:   This secret will be used in all RADIUS queries. If this is not set
    then RADIUS queries will fail.

`radius_authtypes` (string)

:   A comma separated list of supported RADIUS authentication methods
    (`pap` or `chap`), in order of preference (default `pap`).

`radius_bind_min` (short); `radius_bind_max` (short)

:   Define a port range in which to bind sockets used to send and
    receive RADIUS packets. Must be at least RADIUS\_FDS (64) wide.
    Simplifies firewalling of RADIUS ports (default: dynamically
    assigned).

`radius_dae_port` (short)

:   Port for DAE RADIUS (Packet of Death/Disconnect, Change of
    Authorization) requests (default: `3799`).

`allow_duplicate_users` (boolean)

:   Allow multiple logins with the same username. If false (the
    default), any prior session with the same username will be dropped
    when a new session is established.

`guest_account` (string)

:   Allow multiple logins matching this specific username.

`bind_address` (ip address)

:   When the tun interface is created, it is assigned the address
    specified here. If no address is given, 1.1.1.1 is used. Packets
    containing user traffic should be routed via this address if given,
    otherwise the primary address of the machine.

`peer_address` (ip address)

:   Address to send to clients as the default gateway.

`send_garp` (boolean)

:   Determines whether or not to send a gratuitous ARP for the
    bind\_address when the server is ready to handle traffic (default:
    `true`). This value is ignored if BGP is configured.

`throttle_speed` (int)

:   Sets the default speed (in kbits/s) which sessions will be limited
    to. If this is set to 0, then throttling will not be used at all.
    Note: You can set this by the CLI, but changes will not affect
    currently connected users.

`throttle_buckets` (int)

:   Number of token buckets to allocate for throttling. Each throttled
    session requires two buckets (in and out).

`accounting_dir` (string)

:   If set to a directory, then every 5 minutes the current usage for
    every connected use will be dumped to a file in this directory. Each
    file dumped begins with a header, where each line is prefixed by
    `#`. Following the header is a single line for every connected user,
    fields separated by a space.

    The fields are username, ip, qos, uptxoctets, downrxoctets. The qos
    field is 1 if a standard user, and 2 if the user is throttled.

`dump_speed` (boolean)

:   If set to true, then the current bandwidth utilization will be
    logged every second. Even if this is disabled, you can see this
    information by running the `uptime` command on the CLI.

`multi_read_count` (int)

:   Number of packets to read off each of the UDP and TUN fds when
    returned as readable by select (default: 10). Avoids incurring the
    unnecessary system call overhead of select on busy servers.

`scheduler_fifo` (boolean)

:   Sets the scheduling policy for the `l2tpns` process to `SCHED_FIFO`.
    This causes the kernel to immediately preempt any currently running
    `SCHED_OTHER` (normal) process in favour of `l2tpns` when it becomes
    runnable. Ignored on uniprocessor systems.

`lock_pages` (boolean)

:   Keep all pages mapped by the `l2tpns` process in memory.

`icmp_rate` (int)

:   Maximum number of host unreachable ICMP packets to send per second.

`packet_limit` (int)

:   Maximum number of packets of downstream traffic to be handled each
    tenth of a second per session. If zero, no limit is applied
    (default: 0). Intended as a DoS prevention mechanism and not a
    general throttling control (packets are dropped, not queued).

`cluster_address` (ip address)

:   Multicast cluster address (default: 239.192.13.13). See
    [Clustering](#clustering) for more information.

`cluster_port` (udp port)

:   UDP cluster port (default: 32792). See [Clustering](#clustering) for
    more information.

`cluster_interface` (string)

:   Interface for cluster packets (default: eth0)

`cluster_mcast_ttl` (int)

:   TTL for multicast packets (default: 1).

`cluster_hb_interval` (int)

:   Interval in tenths of a second between cluster heartbeat/pings.

`cluster_hb_timeout` (int)

:   Cluster heartbeat timeout in tenths of a second. A new master will
    be elected when this interval has been passed without seeing a
    heartbeat from the master.

`cluster_master_min_adv` (int)

:   Determines the minimum number of up to date slaves required before
    the master will drop routes (default: 1).

`ipv6_prefix` (ipv6 address)

:   Enable negotiation of IPv6. This forms the the first 64 bits of the
    client allocated address. The remaining 64 come from the allocated
    IPv4 address and 4 bytes of 0s.

### BGP {#config-startup-bgp}

BGP routing configuration is entered by the command: router bgp as where
as specifies the local AS number.

Subsequent lines prefixed with neighbour peer define the attributes of
BGP neighhbours. Valid commands are: neighbour peer remote-as as
neighbour peer timers keepalive hold

Where peer specifies the BGP neighbour as either a hostname or IP
address, as is the remote AS number and keepalive, hold are the timer
values in seconds.

### Access Lists {#config-startup-acl}

Named access-lists are configured using one of the commands: ip
access-list standard name ip access-list extended name

Subsequent lines prefixed with `permit` or `deny` define the body of the
access-list. Standard access-list syntax:

{`permit`\|`deny`} {host\|source source-wildcard\|`any`}
\[{host\|destination destination-wildcard\|`any`}\]

Extended access-lists:

{`permit`\|`deny`} `ip` {host\|source source-wildcard\|`any`}
{host\|destination destination-wildcard\|`any`} \[`fragments`\]

{`permit`\|`deny`} `udp` {host\|source source-wildcard\|`any`}
\[{`eq`\|`neq`\|`gt`\|`lt`} port\|`range` from to\] {host\|destination
destination-wildcard\|`any`} \[{`eq`\|`neq`\|`gt`\|`lt`} port\|`range`
from to\] \[`fragments`\]

{`permit`\|`deny`} `tcp` {host\|source source-wildcard\|`any`}
\[{`eq`\|`neq`\|`gt`\|`lt`} port\|`range` from to\] {host\|destination
destination-wildcard\|`any`} \[{`eq`\|`neq`\|`gt`\|`lt`} port\|`range`
from to\] \[{`established`\|{`match-any`\|`match-all`}
{`+`\|`-`}{`fin`\|`syn`\|`rst`\|`psh`\|`ack`\|`urg`} \...\|`fragments`\]

`users` {#config-users}
-------

Usernames and passwords for the command-line interface are stored in
this file. The format is username:password where password may either by
plain text, an MD5 digest (prefixed by `$1`salt`$`) or a DES password,
distinguished from plain text by the prefix `{crypt}`.

The username `enable` has a special meaning and is used to set the
enable password.

::: {.important}
If this file doesn\'t exist, then anyone who can get to port 23 will be
allowed access without a username or password.
:::

`ip_pool` {#config-ip-pool}
---------

This file is used to configure the IP address pool which user addresses
are assigned from. This file should contain either an IP address or a
CIDR network per line. e.g.:

    192.168.1.1
    192.168.1.2
    192.168.1.3
    192.168.4.0/24
    172.16.0.0/16
    10.0.0.0/8

Keep in mind that `l2tpns` can only handle 65535 connections per
process, so don\'t put more than 65535 IP addresses in the configuration
file. They will be wasted.

`build-garden` {#config-build-garden}
--------------

The garden plugin on startup creates a NAT table called \"garden\" then
sources the `build-garden` script to populate that table. All packets
from gardened users will be sent through this table. Example:

    iptables -t nat -A garden -p tcp -m tcp --dport 25 -j DNAT --to 192.168.1.1
    iptables -t nat -A garden -p udp -m udp --dport 53 -j DNAT --to 192.168.1.1
    iptables -t nat -A garden -p tcp -m tcp --dport 53 -j DNAT --to 192.168.1.1
    iptables -t nat -A garden -p tcp -m tcp --dport 80 -j DNAT --to 192.168.1.1
    iptables -t nat -A garden -p tcp -m tcp --dport 110 -j DNAT --to 192.168.1.1
    iptables -t nat -A garden -p tcp -m tcp --dport 443 -j DNAT --to 192.168.1.1
    iptables -t nat -A garden -p icmp -m icmp --icmp-type echo-request -j DNAT --to 192.168.1.1
    iptables -t nat -A garden -p icmp -j ACCEPT
    iptables -t nat -A garden -j DROP

Operation
=========

A running l2tpns process can be controlled in a number of ways. The
primary method of control is by the Command-Line Interface (CLI).

You can also remotely send commands to modules via the `nsctl` client
provided.

There are also a number of signals that l2tpns understands and takes
action when it receives them.

Command-Line Interface {#operation-cli}
----------------------

You can access the command line interface by telneting to port 23. There
is no IP address restriction, so it\'s a good idea to firewall this port
off from anyone who doesn\'t need access to it. See [](#config-users)
for information on restricting access based on a username and password.

The CLI gives you real-time control over almost everything in the
process. The interface is designed to look like a Cisco device, and
supports things like command history, line editing and context sensitive
help. This is provided by linking with the
[libcli](http://sourceforge.net/projects/libcli) library. Some general
documentation of the interface is
[here](http://sourceforge.net/docman/display_doc.php?docid=20501&group_id=79019).

After you have connected to the telnet port (and perhaps logged in), you
will be presented with a `hostname>` prompt.

Enter `help` to get a list of possible commands, or press `?` for
context-specific help.

A brief overview of the more important commands follows:

`show session [ID]
          `

:   Detailed information for a specific session is presented if you
    specify a session ID argument.

    If no ID is given, a summary of all connected sessions is produced.
    Note that this summary list can be around 185 columns wide, so you
    should probably use a wide terminal.

    The columns listed in the summary are:

      -------------- -------------------------------------------------------------------------------------------------------------- ------------------------------------------------------------------------------------------------------------------------------------
      `SID`          Session ID                                                                                                     
      `TID`          Tunnel ID                                                                                                      See also the [show tunnel](#operation-cli-show-tunnel) CLI command.
      `Username`     The username given in the PPP authentication.                                                                  If this is \*, then LCP authentication has not completed.
      `IP`           The IP address given to the session.                                                                           If this is 0.0.0.0, IPCP negotiation has not completed
      `I`            Intercept                                                                                                      Y or N: indicates whether the session is being snooped. See also the [snoop](#operation-cli-snoop) CLI command.
      `T`            Throttled                                                                                                      Y or N: indicates whether the session is currently throttled. See also the [throttle](#operation-cli-throttle) CLI command.
      `G`            Walled Garden                                                                                                  Y or N: indicates whether the user is trapped in the walled garden. This field is present even if the garden module is not loaded.
      `6`            IPv6                                                                                                           Y or N: indicates whether the session has IPv6 active (IPV6CP open)
      `opened`       The number of seconds since the session started                                                                
      `downloaded`   Number of bytes downloaded by the user                                                                         
      `uploaded`     Number of bytes uploaded by the user                                                                           
      `idle`         The number of seconds since traffic was detected on the session                                                
      `LAC`          The IP address of the LAC the session is connected to.                                                         
      `CLI`          The Calling-Line-Identification field provided during the session setup. This field is generated by the LAC.   
      -------------- -------------------------------------------------------------------------------------------------------------- ------------------------------------------------------------------------------------------------------------------------------------

`show users`; `show user username
          `

:   With no arguments, display a list of currently connected users. If
    an argument is given, the session details for the given username are
    displayed.

`show tunnel [ID]`

:   Produce a summary list of all open tunnels, or detail on a specific
    tunnel ID.

    The columns listed in the summary are:

      ---------- -----------------------------------------------------------------------------------------------------------
      TID        Tunnel ID
      Hostname   The hostname for the tunnel as provided by the LAC. This has no relation to DNS, it is just a text field.
      IP         The IP address of the LAC
      State      Tunnel state: Free, Open, Dieing, Opening
      Sessions   The number of open sessions on the tunnel
      ---------- -----------------------------------------------------------------------------------------------------------

`show pool`

:   Displays the current IP address pool allocation. This will only
    display addresses that are in use, or are reserved for re-allocation
    to a disconnected user.

    If an address is not currently in use, but has been used, then in
    the User column the username will be shown in square brackets,
    followed by the time since the address was used:

        IP Address      Used  Session User
        192.168.100.6     N           [joe.user] 1548s

`show radius`

:   Show a summary of the in-use RADIUS sessions. This list should not
    be very long, as RADIUS sessions should be cleaned up as soon as
    they are used. The columns listed are:

      --------- ----------------------------------------------------------------------------------------------------
      Radius    The ID of the RADIUS request. This is sent in the packet to the RADIUS server for identification
      State     The state of the request: WAIT, CHAP, AUTH, IPCP, START, STOP or NULL
      Session   The session ID that this RADIUS request is associated with
      Retry     If a response does not appear to the request, it will retry at this time. This is a Unix timestamp
      Try       Retry count. The RADIUS request is discarded after 3 retries
      --------- ----------------------------------------------------------------------------------------------------

`show running-config`

:   This will list the current running configuration. This is in a
    format that can either be pasted into the configuration file, or run
    directly at the command line.

`show counters`

:   Internally, counters are kept of key values, such as bytes and
    packets transferred, as well as function call counters. This
    function displays all these counters, and is probably only useful
    for debugging.

    You can reset these counters by running `clear counters`.

`show cluster`

:   Show cluster status. Shows the cluster state for this server
    (Master/Slave), information about known peers and (for slaves) the
    master IP address, last packet seen and up-to-date status. See
    [Clustering](#clustering) for more information.

`write memory`

:   This will write the current running configuration to the config file
    `startup-config`, which will be run on a restart.

`snoop user
        IP
        port`

:   You must specify a username, IP address and port. All packets for
    the current session for that username will be forwarded to the given
    host/port. Specify `no snoop
            username` to disable interception for the session.

    If you want interception to be permanent, you will have to modify
    the RADIUS response for the user. See [Interception](#interception).

`throttle user
        [in|out] rate`

:   You must specify a username, which will be throttled for the current
    session to rate Kbps. Prefix rate with `in` or `out` to set
    different upstream and downstream rates.

    Specify `no throttle
            username` to disable throttling for the current session.

    If you want throttling to be permanent, you will have to modify the
    RADIUS response for the user. See [Throttling](#throttling).

`drop session`

:   This will cleanly disconnect the session specified by session ID.

`drop tunnel`

:   This will cleanly disconnect the tunnel specified by tunnel ID, as
    well as all sessions on that tunnel.

`uptime`

:   This will show how long the `l2tpns` process has been running, and
    the current bandwidth utilization:

        17:10:35 up 8 days, 2212 users, load average: 0.21, 0.17, 0.16
        Bandwidth: UDP-ETH:6/6  ETH-UDP:13/13  TOTAL:37.6   IN:3033 OUT:2569

    The bandwidth line contains 4 sets of values:

      ------------ ----------------------------------------------------------------------------------------
      UDP-ETH      The current bandwidth going from the LAC to the ethernet (user uploads), in mbits/sec.
      ETH-UDP      The current bandwidth going from ethernet to the LAC (user downloads).
      TOTAL        The total aggregate bandwidth in mbits/s.
      IN and OUT   Packets/per-second going between UDP-ETH and ETH-UDP.
      ------------ ----------------------------------------------------------------------------------------

    These counters are updated every second.

`configure terminal`

:   Enter configuration mode. Use `exit` or `^Z` to exit this mode.

    The following commands are valid in this mode:

    `load plugin
                name`

    :   Load a plugin. You must specify the plugin name, and it will
        search in `/usr/lib/l2tpns` for `name.so`. You can unload a
        loaded plugin with `remove plugin
                    name`.

    `set` \...

    :   Set a configuration variable. You must specify the variable
        name, and the value. If the value contains any spaces, you
        should quote the value with double (\") or single (\') quotes.

        You can set any configuration value in this way, although some
        may require a restart to take effect. See [](#config-startup).

    `router bgp` \...

    :   Configure BGP. See [BGP](#config-startup-bgp).

    `ip access-list` \...

    :   Configure a named access list. See [Access
        Lists](#config-startup-acl).

nsctl {#operation-nsctl}
-----

`nsctl` sends messages to a running `l2tpns` instance to be control
plugins.

Arguments are `command` and optional args. See `nsctl(8)`.

Built-in command are `load_plugin`, `unload_plugin` and `help`. Any
other commands are passed to plugins for processing by the
`plugin_control` function.

Signals {#operation-signals}
-------

While the process is running, you can send it a few different signals,
using the `kill` command.

    killall -HUP l2tpns

The signals understood are:

SIGHUP

:   Reload the config from disk and re-open log file.

SIGTERM; SIGINT

:   Stop process. Tunnels and sessions are not terminated. This signal
    should be used to stop `l2tpns` on a cluster node where there are
    other machines to continue handling traffic. See
    [Clustering](#clustering)

SIGQUIT

:   Shut down tunnels and sessions, exit process when complete.

Throttling
==========

`l2tpns` contains support for slowing down user sessions to whatever
speed you desire. The global setting `throttle_speed` defines the
default throttle rate.

To throttle a sesion permanently, add a `Cisco-AVPair` RADIUS attribute.
The `autothrotle` module interprets the following attributes:

  ----------------------------------------------- --------------------------------------------------------------------------------
  `throttle=yes`                                  Throttle upstream/downstream traffic to the configured `throttle_speed`.
  `throttle=rate`                                 Throttle upstream/downstream traffic to the specified rate Kbps.
  `lcp:interface-config#1=service-policy input    Alternate (Cisco) format: throttle upstream/downstream to specified rate Kbps.
            rate`                                 
  `lcp:interface-config#2=service-policy output   
            rate`                                 
  ----------------------------------------------- --------------------------------------------------------------------------------

You can also enable and disable throttling an active session using the
[throttle](#operation-cli-throttle) CLI command.

Interception
============

You may have to deal with legal requirements to be able to intercept a
user\'s traffic at any time. `l2tpns` allows you to begin and end
interception on the fly, as well as at authentication time.

When a user is being intercepted, a copy of every packet they send and
receive will be sent wrapped in a UDP packet to a specified host.

The UDP packet contains just the raw IP frame, with no extra headers.
The script `scripts/l2tpns-capture` may be used as the end-point for
such intercepts, writing the data in PCAP format (suitable for
inspection with `tcpdump`).

To enable or disable interception of a connected user, use the
[snoop](#operation-cli-snoop) and `no
      snoop` CLI commands. These will enable interception immediately.

If you wish the user to be intercepted whenever they reconnect, you will
need to modify the RADIUS response to include the Vendor-Specific value
`Cisco-AVPair="intercept=ip:port"`. For this feature to be enabled, you
need to have the `autosnoop` module loaded.

Plugins
=======

So as to make `l2tpns` as flexible as possible, a plugin API is include
which you can use to hook into certain events.

There are a some standard modules included which may be used as
examples: `autosnoop`, `autothrottle`, `garden`, `sessionctl`,
`setrxspeed`, `snoopctl`, `stripdomain` and `throttlectl`.

When an event occurs that has a hook, `l2tpns` looks for a predefined
function name in every loaded module, and runs them in the order the
modules were loaded.

The function should return `PLUGIN_RET_OK` if it is all OK. If it
returns `PLUGIN_RET_STOP`, then it is assumed to have worked, but that
no further modules should be run for this event.

A return of `PLUGIN_RET_ERROR` means that this module failed, and no
further processing should be done for this event.

::: {.note}
Use this with care.
:::

Most event functions take a specific structure named `param_event`,
which varies in content with each event. The function name for each
event will be `plugin_event`, so for the event timer, the function
declaration should look like:

    int plugin_timer(struct param_timer *data);

A list of the available events follows, with a list of all the fields in
the supplied structure:

+----------------------+----------------------+----------------------+
| Event                | Description          | Arguments            |
+======================+======================+======================+
| `plugin_init`        | Called when the      | `s                   |
|                      | plugin is loaded. A  | truct pluginfuncs *` |
|                      | pointer to a struct  |                      |
|                      | containing function  |                      |
|                      | pointers is passed   |                      |
|                      | as the only          |                      |
|                      | argument, allowing   |                      |
|                      | the plugin to call   |                      |
|                      | back into the main   |                      |
|                      | code.                |                      |
|                      |                      |                      |
|                      | Prior to loading the |                      |
|                      | plugin, `l2tpns`     |                      |
|                      | checks the API       |                      |
|                      | version the plugin   |                      |
|                      | was compiled         |                      |
|                      | against. All plugins |                      |
|                      | should contain:      |                      |
|                      |                      |                      |
|                      |     int              |                      |
|                      | plugin_api_version = |                      |
|                      |  PLUGIN_API_VERSION; |                      |
+----------------------+----------------------+----------------------+
| See `pluginfuncs`    |                      |                      |
| structure in         |                      |                      |
| `plugin.h` for       |                      |                      |
| available functions. |                      |                      |
+----------------------+----------------------+----------------------+
| `plugin_done`        | Called when the      | `void`               |
|                      | plugin is unloaded   |                      |
|                      | or `l2tpns` is       |                      |
|                      | shutdown.            |                      |
+----------------------+----------------------+----------------------+
| No arguments.        |                      |                      |
+----------------------+----------------------+----------------------+
| `plugin_pre_auth`    | Called after a       | `struct plug         |
|                      | RADIUS response has  | in param_pre_auth *` |
|                      | been received, but   |                      |
|                      | before it has been   |                      |
|                      | processed by the     |                      |
|                      | code. This will      |                      |
|                      | allow you to modify  |                      |
|                      | the response in some |                      |
|                      | way.                 |                      |
+----------------------+----------------------+----------------------+
| `tunnelt *t`         | Tunnel.              |                      |
+----------------------+----------------------+----------------------+
| `sessiont *s`        | Session.             |                      |
+----------------------+----------------------+----------------------+
| `char *username`     | User name.           |                      |
+----------------------+----------------------+----------------------+
| `char *password`     | Password.            |                      |
+----------------------+----------------------+----------------------+
| `int protocol`       | Authentication       |                      |
|                      | protocol: `0xC023`   |                      |
|                      | for PAP, `0xC223`    |                      |
|                      | for CHAP.            |                      |
+----------------------+----------------------+----------------------+
| `int continue_auth`  | Set to 0 to stop     |                      |
|                      | processing           |                      |
|                      | authentication       |                      |
|                      | modules.             |                      |
+----------------------+----------------------+----------------------+
| `plugin_post_auth`   | Called after a       | `struct plugi        |
|                      | RADIUS response has  | n param_post_auth *` |
|                      | been received, and   |                      |
|                      | the basic checks     |                      |
|                      | have been performed. |                      |
|                      | This is what the     |                      |
|                      | `garden` module uses |                      |
|                      | to force             |                      |
|                      | authentication to be |                      |
|                      | accepted.            |                      |
+----------------------+----------------------+----------------------+
| `tunnelt *t`         | Tunnel.              |                      |
+----------------------+----------------------+----------------------+
| `sessiont *s`        | Session.             |                      |
+----------------------+----------------------+----------------------+
| `char *username`     | User name.           |                      |
+----------------------+----------------------+----------------------+
| `short auth_allowed` | Initially true or    |                      |
|                      | false depending on   |                      |
|                      | whether              |                      |
|                      | authentication has   |                      |
|                      | been allowed so far. |                      |
|                      | You can set this to  |                      |
|                      | 1 or 0 to force      |                      |
|                      | authentication to be |                      |
|                      | accepted or          |                      |
|                      | rejected.            |                      |
+----------------------+----------------------+----------------------+
| `int protocol`       | Authentication       |                      |
|                      | protocol: `0xC023`   |                      |
|                      | for PAP, `0xC223`    |                      |
|                      | for CHAP.            |                      |
+----------------------+----------------------+----------------------+
| `plugin_timer`       | Run once per second. | `struct p            |
|                      |                      | lugin param_timer *` |
+----------------------+----------------------+----------------------+
| `time_t time_now`    | The current unix     |                      |
|                      | timestamp.           |                      |
+----------------------+----------------------+----------------------+
| `plugin_new_session` | Called after a       | `struct plugin       |
|                      | session is fully set | param_new_session *` |
|                      | up. The session is   |                      |
|                      | now ready to handle  |                      |
|                      | traffic.             |                      |
+----------------------+----------------------+----------------------+
| `tunnelt *t`         | Tunnel.              |                      |
+----------------------+----------------------+----------------------+
| `sessiont *s`        | Session.             |                      |
+----------------------+----------------------+----------------------+
| `                    | Called when a        | `struct plugin p     |
| plugin_kill_session` | session is about to  | aram_kill_session *` |
|                      | be shut down. This   |                      |
|                      | may be called        |                      |
|                      | multiple times for   |                      |
|                      | the same session.    |                      |
+----------------------+----------------------+----------------------+
| `tunnelt *t`         | Tunnel.              |                      |
+----------------------+----------------------+----------------------+
| `sessiont *s`        | Session.             |                      |
+----------------------+----------------------+----------------------+
| `plugin_control`     | Called in whenever a | `struct plu          |
|                      | `nsctl` packet is    | gin param_control *` |
|                      | received. This       |                      |
|                      | should handle the    |                      |
|                      | packet and form a    |                      |
|                      | response if          |                      |
|                      | required.            |                      |
|                      |                      |                      |
|                      | Plugin-specific help |                      |
|                      | strings may be       |                      |
|                      | included in the      |                      |
|                      | output of            |                      |
|                      | `nsctl help` by      |                      |
|                      | defining a `NULL`    |                      |
|                      | terminated list of   |                      |
|                      | strings as follows:  |                      |
|                      |                      |                      |
|                      |     char             |                      |
|                      |  *plugin_control_hel |                      |
|                      | p[] = { ..., NULL }; |                      |
+----------------------+----------------------+----------------------+
| `int iam_master`     | If true, this node   |                      |
|                      | is the cluster       |                      |
|                      | master.              |                      |
+----------------------+----------------------+----------------------+
| `int argc`           | `nsctl` arguments.   |                      |
+----------------------+----------------------+----------------------+
| `char **argc`        |                      |                      |
+----------------------+----------------------+----------------------+
| `int response`       | Response from        |                      |
|                      | control message (if  |                      |
|                      | handled): should be  |                      |
|                      | either               |                      |
|                      | `NSCTL_RES_OK` or    |                      |
|                      | `NSCTL_RES_ERR`.     |                      |
+----------------------+----------------------+----------------------+
| `char *additional`   | Additional           |                      |
|                      | information, output  |                      |
|                      | by `nsctl` on        |                      |
|                      | receiving the        |                      |
|                      | response.            |                      |
+----------------------+----------------------+----------------------+
| `plu                 | Called whenever a    | `struct plugin para  |
| gin_radius_response` | RADIUS response      | m_radius_response *` |
|                      | includes a           |                      |
|                      | `Cisco-AVPair`       |                      |
|                      | value. The value is  |                      |
|                      | split into           |                      |
|                      | key`=`value pairs.   |                      |
|                      | Will be called once  |                      |
|                      | for each pair in the |                      |
|                      | response.            |                      |
+----------------------+----------------------+----------------------+
| `tunnelt *t`         | Tunnel.              |                      |
+----------------------+----------------------+----------------------+
| `sessiont *s`        | Session.             |                      |
+----------------------+----------------------+----------------------+
| `char *key`          | Key and value.       |                      |
+----------------------+----------------------+----------------------+
| `char *value`        |                      |                      |
+----------------------+----------------------+----------------------+
| `                    | Called whenever a    | `struct p            |
| plugin_radius_reset` | RADIUS CoA request   | aram_radius_reset *` |
|                      | is received to reset |                      |
|                      | any options to       |                      |
|                      | default values       |                      |
|                      | before the new       |                      |
|                      | values are applied.  |                      |
+----------------------+----------------------+----------------------+
| `tunnelt *t`         | Tunnel.              |                      |
+----------------------+----------------------+----------------------+
| `sessiont *s`        | Session.             |                      |
+----------------------+----------------------+----------------------+
| `pl                  | Called when          | `struct par          |
| ugin_radius_account` | preparing a RADIUS   | am_radius_account *` |
|                      | accounting record to |                      |
|                      | allow additional     |                      |
|                      | data to be added to  |                      |
|                      | the packet.          |                      |
+----------------------+----------------------+----------------------+
| `tunnelt *t`         | Tunnel.              |                      |
+----------------------+----------------------+----------------------+
| `sessiont *s`        | Session.             |                      |
+----------------------+----------------------+----------------------+
| `uint8_t **packet`   | Pointer to the end   |                      |
|                      | of the currently     |                      |
|                      | assembled packet     |                      |
|                      | buffer. The value    |                      |
|                      | should be            |                      |
|                      | incremented by the   |                      |
|                      | length of any data   |                      |
|                      | added.               |                      |
+----------------------+----------------------+----------------------+
| `p                   | Called when a node   | `void`               |
| lugin_become_master` | elects itself        |                      |
|                      | cluster master.      |                      |
+----------------------+----------------------+----------------------+
| No arguments.        |                      |                      |
+----------------------+----------------------+----------------------+
| `plugin              | Called once for each | `sessiont *`         |
| _new_session_master` | open session on      |                      |
|                      | becoming cluster     |                      |
|                      | master.              |                      |
+----------------------+----------------------+----------------------+
| Session.             |                      |                      |
+----------------------+----------------------+----------------------+

Walled Garden
=============

A \"Walled Garden\" is implemented so that you can provide perhaps
limited service to sessions that incorrectly authenticate.

Whenever a session provides incorrect authentication, and the RADIUS
server responds with Auth-Reject, the walled garden module (if loaded)
will force authentication to succeed, but set the `walled_garden` flag
in the session structure, and adds an `iptables` rule to the
`garden_users` chain to cause all packets for the session to traverse
the `garden` chain.

This doesn\'t *just work*. To set this all up, you will to setup the
`garden` nat table with the [build-garden](#config-build-garden) script
with rules to limit user\'s traffic.

For example, to force all traffic except DNS to be forwarded to
192.168.1.1, add these entries to your `build-garden` script:

    iptables -t nat -A garden -p tcp --dport ! 53 -j DNAT --to 192.168.1.1
    iptables -t nat -A garden -p udp --dport ! 53 -j DNAT --to 192.168.1.1

`l2tpns` will add entries to the `garden_users` chain as appropriate.

You can check the amount of traffic being captured using the following
command:

    iptables -t nat -L garden -nvx

Filtering
=========

Sessions may be filtered by specifying `Filter-Id` attributes in the
RADIUS reply. filter.`in` specifies that the named access-list filter
should be applied to traffic from the customer, filter.`out` specifies a
list for traffic to the customer.

Clustering
==========

An `l2tpns` cluster consists of one\* or more servers configured with
the same configuration, notably the multicast `cluster_address` and the
`cluster_port`

\*A stand-alone server is simply a degraded cluster.

Initially servers come up as cluster slaves, and periodically (every
`cluster_hb_interval`/10 seconds) send out ping packets containing the
start time of the process to the multicast `cluster_address` on
`cluster_port`.

A cluster master sends heartbeat rather than ping packets, which contain
those session and tunnel changes since the last heartbeat.

When a slave has not seen a heartbeat within `cluster_hb_timeout`/10
seconds it \"elects\" a new master by examining the list of peers it has
seen pings from and determines which of these and itself is the \"best\"
candidate to be master. \"Best\" in this context means the server with
the highest uptime (the highest IP address is used as a tie-breaker in
the case of equal uptimes).

After discovering a master, and determining that it is up-to-date (has
seen an update for all in-use sessions and tunnels from heartbeat
packets) will raise a route (see [Routing](#routing)) for the
`bind_address` and for all addresses/networks in `ip_pool`.

Any packets recieved by the slave which would alter the session state,
as well as packets for throttled or gardened sessions are forwarded to
the master for handling. In addition, byte counters for session traffic
are periodically forwarded.

The master, when determining that it has at least one\* up-to-date slave
will drop all routes (raising them again if all slaves disappear) and
subsequently handle only packets forwarded to it by the slaves.

\*Configurable with `cluster_master_min_adv`

Multiple clusters can be run on the same network by just using different
multicast `cluster_address`. However, for a given host to be part of
multiple clusters without mixing the clusters, `cluster_port` must be
different for each cluster.

Routing
=======

If you are running a single instance, you may simply statically route
the IP pools to the `bind_address` (`l2tpns` will send a gratuitous
arp).

For a cluster, configure the members as BGP neighbours on your router
and configure multi-path load-balancing. Cisco uses `maximum-paths ibgp`
for IBGP. If this is not supported by your IOS revision, you can use
`maximum-paths` (which works for EBGP) and set `as_number` to a private
value such as 64512.
